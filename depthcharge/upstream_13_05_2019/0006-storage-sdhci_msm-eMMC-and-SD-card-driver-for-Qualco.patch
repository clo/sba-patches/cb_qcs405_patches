From f36b97a88200e7a395f4fb968fc9137dbcad657d Mon Sep 17 00:00:00 2001
From: Veerabhadrarao Badiganti <vbadigan@codeaurora.org>
Date: Fri, 22 Feb 2019 10:50:29 +0530
Subject: [PATCH 06/15] storage: sdhci_msm: eMMC and SD card driver for
 Qualcomm chipsets

Changes for supporting eMMC and SD card driver functionality with
standard SDHC controller on Qualcomm chipsets.

Change-Id: If161e9f56cbcf9140ec5b4c83b72f5040934f320
Signed-off-by: Veerabhadrarao Badiganti <vbadigan@codeaurora.org>
Signed-off-by: Nitheesh Sekar <nsekar@codeaurora.org>
---
 src/drivers/storage/Kconfig      |   6 ++
 src/drivers/storage/Makefile.inc |   1 +
 src/drivers/storage/sdhci_msm.c  | 115 +++++++++++++++++++++++++++++++++++++++
 src/drivers/storage/sdhci_msm.h  |  38 +++++++++++++
 4 files changed, 160 insertions(+)
 create mode 100644 src/drivers/storage/sdhci_msm.c
 create mode 100644 src/drivers/storage/sdhci_msm.h

diff --git a/src/drivers/storage/Kconfig b/src/drivers/storage/Kconfig
index 3fcb5cd26510..a95c62b3849d 100644
--- a/src/drivers/storage/Kconfig
+++ b/src/drivers/storage/Kconfig
@@ -95,4 +95,10 @@ config DRIVER_STORAGE_NVME
 	bool "NVMe driver"
 	default n
 
+config DRIVER_STORAGE_SDHCI_MSM
+	depends on DRIVER_STORAGE_MMC
+	depends on DRIVER_SDHCI
+	bool "MMC driver for Qcom MSM platforms"
+	default n
+
 source src/drivers/storage/mtd/Kconfig
diff --git a/src/drivers/storage/Makefile.inc b/src/drivers/storage/Makefile.inc
index 9e186e8ebab2..817369e6fa2f 100644
--- a/src/drivers/storage/Makefile.inc
+++ b/src/drivers/storage/Makefile.inc
@@ -30,3 +30,4 @@ depthcharge-$(CONFIG_DRIVER_STORAGE_NVME) += nvme.c
 subdirs-y += mtd
 depthcharge-$(CONFIG_DRIVER_STORAGE_MMC_MTK) += mtk_mmc.c bouncebuf.c
 depthcharge-$(CONFIG_DRIVER_STORAGE_MMC_MVMAP2315) += mvmap2315_mmc.c bouncebuf.c
+depthcharge-$(CONFIG_DRIVER_STORAGE_SDHCI_MSM) += sdhci_msm.c
diff --git a/src/drivers/storage/sdhci_msm.c b/src/drivers/storage/sdhci_msm.c
new file mode 100644
index 000000000000..26753656d67f
--- /dev/null
+++ b/src/drivers/storage/sdhci_msm.c
@@ -0,0 +1,115 @@
+/*
+ * Copyright (C) 2019, The Linux Foundation.  All rights reserved.
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License version 2 and
+ * only version 2 as published by the Free Software Foundation.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ */
+
+#include <libpayload.h>
+
+#include "drivers/storage/sdhci_msm.h"
+
+/*
+ * msm specific SDHC initialization
+ */
+static int sdhci_msm_init(SdhciHost *host)
+{
+	u32 vendor_caps = 0, config = 0;
+
+	mmc_debug("Initializing SDHCI MSM host controller!\n")
+
+	/* Read host controller capabilities */
+	vendor_caps = sdhci_readl(host, SDHCI_CAPABILITIES);
+
+	/* Fix the base clock to max clock supported */
+	vendor_caps &= ~SDHCI_CLOCK_V3_BASE_MASK;
+	vendor_caps |= (host->clock_f_max/(1*MHz)) << SDHCI_CLOCK_BASE_SHIFT;
+
+	/*
+	 * Explicitly enable the capabilities which are not advertised
+	 * by default
+	 */
+	if (host->removable)
+		vendor_caps |= SDHCI_CAN_VDD_300;
+	else
+		vendor_caps |= SDHCI_CAN_VDD_180 | SDHCI_CAN_DO_8BIT;
+
+	/*
+	 * Update internal capabilities register so that these updated values
+	 * will get reflected in SDHCI_CAPABILITEIS register.
+	 */
+	sdhci_writel(host, vendor_caps, SDCC_HC_VENDOR_SPECIFIC_CAPABILITIES0);
+
+	/* Set the PAD_PWR_SWITCH_EN bit. And set it based on host capability */
+	config = VENDOR_SPEC_FUN1_POR_VAL | HC_IO_PAD_PWR_SWITCH_EN;
+	if (vendor_caps & SDHCI_CAN_VDD_300)
+		config &= ~HC_IO_PAD_PWR_SWITCH;
+	else if (vendor_caps & SDHCI_CAN_VDD_180)
+		config |= HC_IO_PAD_PWR_SWITCH;
+
+	/*
+	 * Disable HC_SELECT_IN to be able to use the UHS mode select
+	 * configuration from HOST_CONTROL2 register for all other modes.
+	 *
+	 * For disabling it, Write 0 to HC_SELECT_IN and HC_SELECT_IN_EN
+	 * fields in VENDOR_SPEC_FUNC1 register.
+	 */
+	config &= ~HC_SELECT_IN_EN & ~HC_SELECT_IN_MASK;
+	sdhci_writel(host, config, SDCC_HC_VENDOR_SPECIFIC_FUNC1);
+
+	/*
+	 * Reset the vendor spec register to power on reset state.
+	 * This is to ensure that this register is set to right value
+	 * incase if this register get updated by bootrom when using SDHCI boot.
+	 */
+	sdhci_writel(host, VENDOR_SPEC_FUN3_POR_VAL,
+		     SDCC_HC_VENDOR_SPECIFIC_FUNC3);
+
+	/*
+	 * Set SD power off, otherwise reset will result in pwr irq.
+	 * And without setting bus off status, reset would fail.
+	 */
+	sdhci_writeb(host, 0x0, SDHCI_POWER_CONTROL);
+	udelay(10);
+
+	return 0;
+}
+
+
+/*
+ * This function will get invoked from board_setup()
+ */
+SdhciHost *new_sdhci_msm_host(void *ioaddr, int platform_info, int clock_max,
+			      void *tlmmAddr, GpioOps *cd_gpio)
+{
+	SdhciHost *host;
+
+	mmc_debug("Max-Clock = %dMHz, Platform-Info = 0x%08x\n",
+		  clock_max/MHz, platform_info);
+
+	host = new_mem_sdhci_host(ioaddr, platform_info, 400*KHz, clock_max, 0);
+	if (!host) {
+		mmc_error("%s: Failed to allocate SdhciHost\n", __func__);
+		goto out;
+	}
+
+	host->attach = sdhci_msm_init;
+	host->cd_gpio = cd_gpio;
+
+	/* We don't need below quirk, so clear it */
+	host->quirks &= ~SDHCI_QUIRK_NO_SIMULT_VDD_AND_POWER;
+
+	/* Configure drive strengths of interface lines */
+	if (host->removable)
+		writel(SDC2_TLMM_CONFIG, tlmmAddr);
+	else
+		writel(SDC1_TLMM_CONFIG, tlmmAddr);
+out:
+	return host;
+}
diff --git a/src/drivers/storage/sdhci_msm.h b/src/drivers/storage/sdhci_msm.h
new file mode 100644
index 000000000000..ea29257a9342
--- /dev/null
+++ b/src/drivers/storage/sdhci_msm.h
@@ -0,0 +1,38 @@
+/*
+ * Copyright (C) 2019, The Linux Foundation.  All rights reserved.
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License version 2 and
+ * only version 2 as published by the Free Software Foundation.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ */
+
+#ifndef __DRIVERS_STORAGE_SDHCI_MSM_H__
+#define __DRIVERS_STORAGE_SDHCI_MSM_H__
+
+#include "drivers/storage/sdhci.h"
+#include "drivers/gpio/gpio.h"
+
+/* SDM specific defines */
+#define SDC1_TLMM_CONFIG	0x9FE4
+#define SDC2_TLMM_CONFIG	0x1FE4
+
+/* SDHC specific defines */
+#define SDCC_HC_VENDOR_SPECIFIC_FUNC1	0x20C
+#define VENDOR_SPEC_FUN1_POR_VAL	0xA0C
+#define HC_IO_PAD_PWR_SWITCH_EN		(1 << 15)
+#define HC_IO_PAD_PWR_SWITCH		(1 << 16)
+#define HC_SELECT_IN_EN			(1 << 18)
+#define HC_SELECT_IN_MASK		(7 << 19)
+#define SDCC_HC_VENDOR_SPECIFIC_FUNC3	0x250
+#define VENDOR_SPEC_FUN3_POR_VAL	0x02226040
+#define SDCC_HC_VENDOR_SPECIFIC_CAPABILITIES0	0x21C
+
+SdhciHost *new_sdhci_msm_host(void *ioaddr, int platform_info, int clock_max,
+			      void *tlmmAddr, GpioOps *cd_gpio);
+
+#endif /* __DRIVERS_STORAGE_SDHCI_MSM_H__ */
-- 
The Qualcomm Innovation Center, Inc. is a member of the Code Aurora Forum,
a Linux Foundation Collaborative Project

