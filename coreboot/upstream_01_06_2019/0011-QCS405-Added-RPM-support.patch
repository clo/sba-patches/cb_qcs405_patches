From ab83c6a3107ba726cdff5863e330921bd3ff31df Mon Sep 17 00:00:00 2001
From: Nitheesh Sekar <nsekar@codeaurora.org>
Date: Thu, 15 Nov 2018 19:19:51 +0530
Subject: [PATCH 11/32] QCS405: Added RPM support

This patch adds support to read RPM image from
3rdparty/blobs and load it. It takes RPM out of reset.

Note that, clock_reset_rpm function to touch the
GCC registers actually should reside in clock.c,
but for now keeping it here till clock patches
are posted.

Change-Id: I17f491f0a4bd0dce7522b7e80e1bac97ec18b945
Signed-off-by: Nitheesh Sekar <nsekar@codeaurora.org>
Signed-off-by: Sricharan R <sricharan@codeaurora.org>
---
 src/soc/qualcomm/qcs405/Makefile.inc             | 12 +++++
 src/soc/qualcomm/qcs405/include/soc/memlayout.ld | 10 ++++-
 src/soc/qualcomm/qcs405/include/soc/rpm.h        | 21 +++++++++
 src/soc/qualcomm/qcs405/include/soc/symbols.h    |  4 ++
 src/soc/qualcomm/qcs405/mmu.c                    |  4 +-
 src/soc/qualcomm/qcs405/rpm_load_reset.c         | 56 ++++++++++++++++++++++++
 src/soc/qualcomm/qcs405/soc.c                    |  3 +-
 7 files changed, 106 insertions(+), 4 deletions(-)
 create mode 100644 src/soc/qualcomm/qcs405/include/soc/rpm.h
 create mode 100644 src/soc/qualcomm/qcs405/rpm_load_reset.c

diff --git a/src/soc/qualcomm/qcs405/Makefile.inc b/src/soc/qualcomm/qcs405/Makefile.inc
index 1f38273f77ff..a4b986ef16fb 100644
--- a/src/soc/qualcomm/qcs405/Makefile.inc
+++ b/src/soc/qualcomm/qcs405/Makefile.inc
@@ -51,6 +51,7 @@ ramstage-y += qup.c
 ramstage-y += blsp.c
 ramstage-y += usb.c
 ramstage-$(CONFIG_DRIVERS_UART) += uart.c
+ramstage-y += rpm_load_reset.c
 
 ################################################################################
 
@@ -107,5 +108,16 @@ ifneq (,$(findstring $(PMIC_FILE),$(pmic_file)))
 	cbfs-files-y += $(PMIC_CBFS)
 endif
 
+################################################################################
+RPM_FILE := $(QCS405_BLOB)/rpm.mbn
+rpm_file := $(shell ls $(RPM_FILE))
+ifneq (,$(findstring $(RPM_FILE),$(rpm_file)))
+	RPM_CBFS := $(CONFIG_CBFS_PREFIX)/rpm
+	$(RPM_CBFS)-file := $(RPM_FILE)
+	$(RPM_CBFS)-type := payload
+	$(RPM_CBFS)-compression := $(CBFS_COMPRESS_FLAG)
+	cbfs-files-y += $(RPM_CBFS)
+endif
+
 endif
 
diff --git a/src/soc/qualcomm/qcs405/include/soc/memlayout.ld b/src/soc/qualcomm/qcs405/include/soc/memlayout.ld
index 237aa4070432..4505b11ec5ae 100644
--- a/src/soc/qualcomm/qcs405/include/soc/memlayout.ld
+++ b/src/soc/qualcomm/qcs405/include/soc/memlayout.ld
@@ -24,8 +24,17 @@
 #define BSRAM_START(addr) SYMBOL(bsram, addr)
 #define BSRAM_END(addr) SYMBOL(ebsram, addr)
 
+/* RPM : 0x0B000000 - 0x0B100000 */
+#define RPMSRAM_START(addr) SYMBOL(rpmsram, addr)
+#define RPMSRAM_END(addr) SYMBOL(rpmsram, addr)
+
 SECTIONS
 {
+	RPMSRAM_START(0x00200000)
+	REGION(pmic, 0x00207000, 0x10000, 4096)
+	REGION(rpm, 0x0021f000, 0x2c000, 0x100)
+	RPMSRAM_END(0x02A4000)
+
 	SSRAM_START(0x8600000)
 	REGION(qcsdi, 0x8600000, 0x8000, 4096)
 	SSRAM_END(0x8608000)
@@ -41,7 +50,6 @@ SECTIONS
 	PRERAM_CBMEM_CONSOLE(0x8C50400, 32K)
 	PRERAM_CBFS_CACHE(0x8C58400, 70K)
 	REGION(qclib, 0x8C6a000, 0x80000, 4096)
-	REGION(pmic, 0x8CED000, 0x10000, 4096)
 	REGION(qclib_serial_log, 0x8cfd000, 0x1000, 0x1000)
 	REGION(ddr_training, 0x8cfe000, 0x2000, 0x1000)
 	REGION(ddr_information, 0x8d00000, 0x100, 0x100)
diff --git a/src/soc/qualcomm/qcs405/include/soc/rpm.h b/src/soc/qualcomm/qcs405/include/soc/rpm.h
new file mode 100644
index 000000000000..e83253fea4ec
--- /dev/null
+++ b/src/soc/qualcomm/qcs405/include/soc/rpm.h
@@ -0,0 +1,21 @@
+/*
+ * This file is part of the coreboot project.
+ *
+ * Copyright (C) 2018, The Linux Foundation.  All rights reserved.
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License version 2 and
+ * only version 2 as published by the Free Software Foundation.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ */
+
+#ifndef _SOC_QUALCOMM_QCS405_RPM_H__
+#define _SOC_QUALCOMM_QCS405_RPM_H__
+
+void rpm_fw_load_reset(void);
+
+#endif  // _SOC_QUALCOMM_QCS405_RPM_H__
diff --git a/src/soc/qualcomm/qcs405/include/soc/symbols.h b/src/soc/qualcomm/qcs405/include/soc/symbols.h
index ab5bdcb1ff08..c0e2dcfee5a0 100644
--- a/src/soc/qualcomm/qcs405/include/soc/symbols.h
+++ b/src/soc/qualcomm/qcs405/include/soc/symbols.h
@@ -25,4 +25,8 @@ DECLARE_REGION(dram_reserved);
 DECLARE_REGION(dcb);
 DECLARE_REGION(pmic);
 
+extern u8 _rpm[];
+extern u8 _erpm[];
+#define _rpm_size (_erpm - _rpm)
+
 #endif // _SOC_QUALCOMM_QCS405_SYMBOLS_H_
diff --git a/src/soc/qualcomm/qcs405/mmu.c b/src/soc/qualcomm/qcs405/mmu.c
index 876732aba031..f10d066c54bd 100644
--- a/src/soc/qualcomm/qcs405/mmu.c
+++ b/src/soc/qualcomm/qcs405/mmu.c
@@ -29,12 +29,12 @@ void qcs405_mmu_init(void)
 	mmu_config_range((void *)_ssram, REGION_SIZE(ssram), MA_MEM | MA_S | MA_RW);
 	mmu_config_range((void *)_bsram, REGION_SIZE(bsram), MA_MEM | MA_S | MA_RW);
 	mmu_config_range((void *)0x80000000, 0x40000000, MA_MEM | MA_S | MA_RW);
+	mmu_config_range((void *)_rpm, _rpm_size, MA_MEM | MA_NS | MA_RW);
 
 	mmu_enable();
 }
 
 void soc_mmu_dram_config_post_dram_init(void)
 {
-	mmu_config_range((void *)0x80000000, 0x40000000,
-			MA_MEM | MA_NS | MA_RW);
+	mmu_config_range((void *)_rpm, _rpm_size, MA_MEM | MA_NS | MA_RW);
 }
diff --git a/src/soc/qualcomm/qcs405/rpm_load_reset.c b/src/soc/qualcomm/qcs405/rpm_load_reset.c
new file mode 100644
index 000000000000..128d706be16e
--- /dev/null
+++ b/src/soc/qualcomm/qcs405/rpm_load_reset.c
@@ -0,0 +1,56 @@
+/*
+ * This file is part of the coreboot project.
+ *
+ * Copyright (C) 2018, The Linux Foundation.  All rights reserved.
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License version 2 and
+ * only version 2 as published by the Free Software Foundation.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ */
+
+#include <device/mmio.h>
+#include <string.h>
+#include <arch/cache.h>
+#include <cbfs.h>
+#include <halt.h>
+#include <console/console.h>
+#include <timestamp.h>
+#include <soc/mmu.h>
+#include <soc/rpm.h>
+#include <soc/clock.h>
+
+#define GCC_APSS_MISC		0x1860000
+#define RPM_RESET		0
+
+static void clock_reset_rpm(void)
+{
+	/* Bring RPM out of RESET */
+	clrbits_le32((void *)(GCC_APSS_MISC), BIT(RPM_RESET));
+}
+
+void rpm_fw_load_reset(void)
+{
+	bool rpm_fw_entry;
+
+	struct prog rpm_fw_prog =
+		PROG_INIT(PROG_PAYLOAD, CONFIG_CBFS_PREFIX "/rpm");
+	printk(BIOS_DEBUG, "\nNIT:PROG_INIT for /rpm has happened.\n");
+
+	if (prog_locate(&rpm_fw_prog))
+		die("SOC image: RPM_FW not found");
+	printk(BIOS_DEBUG, "\nNIT:PROG_LOCATE for /rpm has happened.\n");
+
+	rpm_fw_entry = selfload(&rpm_fw_prog);
+	if (!rpm_fw_entry)
+		die("SOC image: RPM load failed");
+	printk(BIOS_DEBUG, "\nNIT:SELF_LOAD of rpm has happened.\n");
+
+	clock_reset_rpm();
+
+	printk(BIOS_DEBUG, "\nSOC:RPM brought out of reset.\n");
+}
diff --git a/src/soc/qualcomm/qcs405/soc.c b/src/soc/qualcomm/qcs405/soc.c
index b3bfb9918aec..693ce7a872f6 100644
--- a/src/soc/qualcomm/qcs405/soc.c
+++ b/src/soc/qualcomm/qcs405/soc.c
@@ -18,6 +18,7 @@
 #include <timestamp.h>
 #include <soc/mmu.h>
 #include <soc/symbols.h>
+#include <soc/rpm.h>
 
 static void soc_read_resources(struct device *dev)
 {
@@ -28,7 +29,7 @@ static void soc_read_resources(struct device *dev)
 
 static void soc_init(struct device *dev)
 {
-
+	rpm_fw_load_reset();
 }
 
 static struct device_operations soc_ops = {
-- 
The Qualcomm Innovation Center, Inc. is a member of the Code Aurora Forum,
a Linux Foundation Collaborative Project

