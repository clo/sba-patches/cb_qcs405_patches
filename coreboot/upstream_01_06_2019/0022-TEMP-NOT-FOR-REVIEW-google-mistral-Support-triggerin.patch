From bf9e5374432b92f0ed318fa8c980f64d24aa6204 Mon Sep 17 00:00:00 2001
From: santhosh hassan <sahassan@google.com>
Date: Wed, 30 Jan 2019 23:01:23 -0800
Subject: [PATCH 22/32] TEMP: NOT FOR REVIEW: google/mistral: Support
 triggering Recovery/FDR

Implement triggering factory data reset and recovery mechanism
based on Recovery/FDR switch(GPIO77) value.

Change-Id: Ifdbd696ee3fd9f073cc44a49ad341cad7e33845e
Signed-off-by: Santhosh Hassan <sahassan@google.com>
Signed-off-by: Nitheesh Sekar <nsekar@codeaurora.org>
---
 src/mainboard/google/mistral/Kconfig      |   6 +-
 src/mainboard/google/mistral/Makefile.inc |   1 -
 src/mainboard/google/mistral/board.h      |  29 ++++++++
 src/mainboard/google/mistral/chromeos.c   | 112 ++++++++++++++++++++++++++++++
 src/mainboard/google/mistral/verstage.c   |   2 +
 5 files changed, 145 insertions(+), 5 deletions(-)
 create mode 100644 src/mainboard/google/mistral/board.h

diff --git a/src/mainboard/google/mistral/Kconfig b/src/mainboard/google/mistral/Kconfig
index f987123d8e05..87d2df8613ab 100644
--- a/src/mainboard/google/mistral/Kconfig
+++ b/src/mainboard/google/mistral/Kconfig
@@ -18,10 +18,8 @@ config BOARD_SPECIFIC_OPTIONS
 
 config VBOOT
 	select VBOOT_VBNV_FLASH
-	select VBOOT_NO_BOARD_SUPPORT
-	select GBB_FLAG_DEV_SCREEN_SHORT_DELAY
-	select GBB_FLAG_FORCE_DEV_SWITCH_ON
-	select GBB_FLAG_FORCE_MANUAL_RECOVERY
+	select VBOOT_WIPEOUT_SUPPORTED
+	select VBOOT_DISABLE_DEV_ON_RECOVERY
 
 config MAINBOARD_DIR
 	string
diff --git a/src/mainboard/google/mistral/Makefile.inc b/src/mainboard/google/mistral/Makefile.inc
index fcfed7dfe1f0..63bb49def936 100644
--- a/src/mainboard/google/mistral/Makefile.inc
+++ b/src/mainboard/google/mistral/Makefile.inc
@@ -1,6 +1,5 @@
 
 bootblock-y += memlayout.ld
-bootblock-y += chromeos.c
 bootblock-y += reset.c
 bootblock-y += bootblock.c
 
diff --git a/src/mainboard/google/mistral/board.h b/src/mainboard/google/mistral/board.h
new file mode 100644
index 000000000000..2e23ce317832
--- /dev/null
+++ b/src/mainboard/google/mistral/board.h
@@ -0,0 +1,29 @@
+/*
+ * This file is part of the coreboot project.
+ *
+ * Copyright (C) 2018, The Linux Foundation.  All rights reserved.
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License version 2 and
+ * only version 2 as published by the Free Software Foundation.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ */
+
+#ifndef __COREBOOT_SRC_MAINBOARD_GOOGLE_MISTRAL_BOARD_H
+#define __COREBOOT_SRC_MAINBOARD_GOOGLE_MISTRAL_BOARD_H
+
+#include <gpio.h>
+#include <soc/gpio.h>
+
+
+#define GPIO_H1_AP_INT	GPIO(53)
+#define GPIO_WP_STATE	GPIO(54)
+#define GPIO_REC_STATE	GPIO(77)
+
+void setup_chromeos_gpios(void);
+
+#endif  /* ! __COREBOOT_SRC_MAINBOARD_GOOGLE_MISTRAL_BOARD_H */
diff --git a/src/mainboard/google/mistral/chromeos.c b/src/mainboard/google/mistral/chromeos.c
index 538e46fa4b43..838691bf884c 100644
--- a/src/mainboard/google/mistral/chromeos.c
+++ b/src/mainboard/google/mistral/chromeos.c
@@ -14,8 +14,120 @@
  */
 
 #include <boot/coreboot_tables.h>
+#include <bootmode.h>
+#include <console/console.h>
+#include <security/tpm/tis.h>
+#include <timer.h>
+#include "board.h"
+
+void setup_chromeos_gpios(void)
+{
+	gpio_input(GPIO_WP_STATE);
+	gpio_input(GPIO_REC_STATE);
+}
 
 void fill_lb_gpios(struct lb_gpios *gpios)
 {
+	struct lb_gpio chromeos_gpios[] = {
+		{GPIO_REC_STATE.addr, ACTIVE_LOW,
+			gpio_get(GPIO_REC_STATE), "recovery"},
+		{GPIO_WP_STATE.addr, ACTIVE_LOW,
+			gpio_get(GPIO_WP_STATE), "write protect"},
+		{-1, ACTIVE_LOW, 1, "power"},
+		{-1, ACTIVE_LOW, 0, "lid"},
+	};
+
+	lb_add_gpios(gpios, chromeos_gpios, ARRAY_SIZE(chromeos_gpios));
+}
+
+#define WIPEOUT_MODE_DELAY_MS (8 * 1000)
+#define RECOVERY_MODE_EXTRA_DELAY_MS (8 * 1000)
+
+/*
+ * The recovery switch: it needs to be pressed for a
+ * certain duration at startup to signal different requests:
+ *
+ * - keeping it pressed for 8 to 16 seconds after startup signals the need for
+ *   factory reset (wipeout);
+ * - keeping it pressed for longer than 16 seconds signals the need for Chrome
+ *   OS recovery.
+ *
+ * The state is read once and cached for following inquiries. The below enum
+ * lists possible states.
+ */
+enum switch_state {
+	not_probed = -1,
+	no_req,
+	recovery_req,
+	wipeout_req
+};
+
+static enum switch_state get_rec_sw_state(void)
+{
+	struct stopwatch sw;
+	int sampled_value;
+	gpio_t rec_sw;
+	static enum switch_state saved_state = not_probed;
+
+	if (saved_state != not_probed)
+		return saved_state;
+
+	rec_sw = GPIO_REC_STATE;
+	sampled_value = !gpio_get(rec_sw);
+
+	if (!sampled_value) {
+		saved_state = no_req;
+		//display_pattern(WWR_NORMAL_BOOT);
+		return saved_state;
+	}
+
+	//display_pattern(WWR_RECOVERY_PUSHED);
+	printk(BIOS_INFO, "recovery button pressed\n");
+
+	stopwatch_init_msecs_expire(&sw, WIPEOUT_MODE_DELAY_MS);
+
+	do {
+		sampled_value = !gpio_get(rec_sw);
+		if (!sampled_value)
+			break;
+	} while (!stopwatch_expired(&sw));
+
+	if (sampled_value) {
+		//display_pattern(WWR_WIPEOUT_REQUEST);
+		printk(BIOS_INFO, "wipeout requested, checking recovery\n");
+		stopwatch_init_msecs_expire(&sw, RECOVERY_MODE_EXTRA_DELAY_MS);
+		do {
+			sampled_value = !gpio_get(rec_sw);
+			if (!sampled_value)
+				break;
+		} while (!stopwatch_expired(&sw));
+
+		if (sampled_value) {
+			saved_state = recovery_req;
+			//display_pattern(WWR_RECOVERY_REQUEST);
+			printk(BIOS_INFO, "recovery requested\n");
+		} else {
+			saved_state = wipeout_req;
+		}
+	} else {
+		saved_state = no_req;
+		//display_pattern(WWR_NORMAL_BOOT);
+	}
+
+	return saved_state;
+}
+
+int get_recovery_mode_switch(void)
+{
+	return get_rec_sw_state() == recovery_req;
+}
 
+int get_wipeout_mode_switch(void)
+{
+	return get_rec_sw_state() == wipeout_req;
+}
+
+int get_write_protect_state(void)
+{
+	return !gpio_get(GPIO_WP_STATE);
 }
diff --git a/src/mainboard/google/mistral/verstage.c b/src/mainboard/google/mistral/verstage.c
index a34e4fa361f3..08b5391166a4 100644
--- a/src/mainboard/google/mistral/verstage.c
+++ b/src/mainboard/google/mistral/verstage.c
@@ -18,11 +18,13 @@
 #include <security/vboot/vboot_common.h>
 #include <soc/clock.h>
 #include <spi-generic.h>
+#include "board.h"
 
 void verstage_mainboard_init(void)
 {
 	struct spi_slave spi;
 
+	setup_chromeos_gpios();
 	printk(BIOS_ERR, "Trying to initialize TPM SPI bus\n");
 	if (spi_setup_slave(CONFIG_DRIVER_TPM_SPI_BUS,
 			    CONFIG_DRIVER_TPM_SPI_CHIP, &spi)) {
-- 
The Qualcomm Innovation Center, Inc. is a member of the Code Aurora Forum,
a Linux Foundation Collaborative Project

