From 84db3f61e95fd41bd47399a39595095a94385b1d Mon Sep 17 00:00:00 2001
From: Nitheesh Sekar <nsekar@codeaurora.org>
Date: Fri, 8 Mar 2019 17:53:03 +0530
Subject: [PATCH 23/26] TEMP: NOT FOR REVIEW: qcom: Add board id detection
 using TPM

This patch adds support to select the board id
based on the TPM availability.

Change-Id: Ifa7b17085364bb631f43a133839773033721062d
Signed-off-by: Nitheesh Sekar <nsekar@codeaurora.org>
---
 src/drivers/spi/tpm/tis.c                        | 12 ++++++++++++
 src/drivers/spi/tpm/tpm.c                        |  5 +++++
 src/mainboard/google/mistral/boardid.c           | 18 +++++++++++++++++-
 src/security/tpm/tis.h                           | 10 ++++++++++
 src/soc/qualcomm/qcs405/include/soc/memlayout.ld |  1 +
 src/soc/qualcomm/qcs405/include/soc/symbols.h    |  4 ++++
 6 files changed, 49 insertions(+), 1 deletion(-)

diff --git a/src/drivers/spi/tpm/tis.c b/src/drivers/spi/tpm/tis.c
index b50ab0a88cf9..d646adbde6d1 100644
--- a/src/drivers/spi/tpm/tis.c
+++ b/src/drivers/spi/tpm/tis.c
@@ -4,6 +4,8 @@
  * found in the LICENSE file.
  */
 
+#include <symbols.h>
+#include <soc/symbols.h>
 #include <arch/early_variables.h>
 #include <console/console.h>
 #include <security/tpm/tis.h>
@@ -92,3 +94,13 @@ int tis_sendrecv(const uint8_t *sendbuf, size_t sbuf_size,
 
 	return 0;
 }
+
+unsigned int is_tpm_detected(void)
+{
+	if (0 == (*_tpm_detection))
+		return 0;
+	else if (1 == (*_tpm_detection))
+		return 1;
+	else
+		return 0;
+}
diff --git a/src/drivers/spi/tpm/tpm.c b/src/drivers/spi/tpm/tpm.c
index aad7610313b5..b2484aa0c0bb 100644
--- a/src/drivers/spi/tpm/tpm.c
+++ b/src/drivers/spi/tpm/tpm.c
@@ -15,6 +15,8 @@
  * Specification Revision 00.43".
  */
 
+#include <symbols.h>
+#include <soc/symbols.h>
 #include <arch/early_variables.h>
 #include <assert.h>
 #include <commonlib/endian.h>
@@ -457,10 +459,13 @@ int tpm2_init(struct spi_slave *spi_if)
 	}
 
 	if (!retries) {
+		*_tpm_detection = 0;
 		printk(BIOS_ERR, "\n%s: Failed to connect to the TPM\n",
 		       __func__);
 		return -1;
 	}
+	else
+		*_tpm_detection = 1;
 
 	printk(BIOS_INFO, " done!\n");
 
diff --git a/src/mainboard/google/mistral/boardid.c b/src/mainboard/google/mistral/boardid.c
index 7fba34be5fd5..66309c0977d4 100644
--- a/src/mainboard/google/mistral/boardid.c
+++ b/src/mainboard/google/mistral/boardid.c
@@ -17,6 +17,7 @@
 #include <gpio.h>
 #include <console/console.h>
 #include <stdlib.h>
+#include <security/tpm/tis.h>
 
 /*
  * Mistral boards dedicate to the board ID three GPIOs in ternary mode: 105, 106
@@ -32,7 +33,22 @@ static uint32_t get_board_id(void)
 				[1] = GPIO(106),
 				[0] = GPIO(105)};
 
-	bid = gpio_binary_first_base3_value(pins, ARRAY_SIZE(pins));
+	if (0 == is_tpm_detected()) {
+		bid = 25;        /* Assign 25 for EVB boards */
+		printk(BIOS_DEBUG, "EVB Board ID: %d\n", bid);
+	} else {
+		bid = 0;         /* Assign 0 for Proto boards */
+
+		/* The board id assigned for Proto boards is 0.
+		 * Since gpios are not wired in the initial phase,
+		 * we will get 0 whihch is a coincidence.
+		 * To make sure it starts working, after gpios are
+		 * wired, reassign the value read from gpios to id.
+		 */
+		bid = gpio_binary_first_base3_value(pins, ARRAY_SIZE(pins));
+
+		printk(BIOS_DEBUG, "Proto Board ID: %d\n", bid);
+	}
 
 	return bid;
 }
diff --git a/src/security/tpm/tis.h b/src/security/tpm/tis.h
index c410838fc21a..dd2f3482dcce 100644
--- a/src/security/tpm/tis.h
+++ b/src/security/tpm/tis.h
@@ -97,4 +97,14 @@ int tis_sendrecv(const u8 *sendbuf, size_t send_size, u8 *recvbuf,
  */
 int tis_plat_irq_status(void);
 
+/*
+ * This function is_tpm_detected() sets tpm_detection value to be 1 upon TPM detection.
+ * The value is writted to the specific region of SRAM which will retain
+ * the TPM Detection information between various stages of the bootloader.
+ *
+ * Returns 0 if TPM is not detected
+ * Returns 1 if TPM is detected
+ */
+unsigned int is_tpm_detected(void);
+
 #endif /* TIS_H_ */
diff --git a/src/soc/qualcomm/qcs405/include/soc/memlayout.ld b/src/soc/qualcomm/qcs405/include/soc/memlayout.ld
index 4505b11ec5ae..12bbde8de1a0 100644
--- a/src/soc/qualcomm/qcs405/include/soc/memlayout.ld
+++ b/src/soc/qualcomm/qcs405/include/soc/memlayout.ld
@@ -54,6 +54,7 @@ SECTIONS
 	REGION(ddr_training, 0x8cfe000, 0x2000, 0x1000)
 	REGION(ddr_information, 0x8d00000, 0x100, 0x100)
 	REGION(dcb, 0x8d01000, 0x1000, 0x1000)
+	REGION(tpm_detection, 0x8d02000, 0x8, 0x10)
 	REGION(qclib_shared_data, 0x8d78000, 0x8000, 0x1000)
 	BSRAM_END(0x8D80000)
 
diff --git a/src/soc/qualcomm/qcs405/include/soc/symbols.h b/src/soc/qualcomm/qcs405/include/soc/symbols.h
index c0e2dcfee5a0..77f536186663 100644
--- a/src/soc/qualcomm/qcs405/include/soc/symbols.h
+++ b/src/soc/qualcomm/qcs405/include/soc/symbols.h
@@ -29,4 +29,8 @@ extern u8 _rpm[];
 extern u8 _erpm[];
 #define _rpm_size (_erpm - _rpm)
 
+extern u8 _tpm_detection[];
+extern u8 _etpm_detection[];
+#define _tpm_detection_size (_etpm_detection - _tpm_detection)
+
 #endif // _SOC_QUALCOMM_QCS405_SYMBOLS_H_
-- 
The Qualcomm Innovation Center, Inc. is a member of the Code Aurora Forum,
a Linux Foundation Collaborative Project

